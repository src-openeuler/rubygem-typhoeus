%global gem_name typhoeus
Name:                rubygem-%{gem_name}
Version:             1.4.1
Release:             1
Summary:             Parallel HTTP library on top of libcurl multi
License:             MIT
URL:                 https://github.com/typhoeus/typhoeus
Source0:             https://rubygems.org/gems/typhoeus-%{version}.gem
#https://github.com/typhoeus/typhoeus/pull/668
Patch0:              typhoeus-1.4.0-Fix-Ruby-3-0-compatibility.patch
BuildRequires:       ruby(release) rubygems-devel ruby rubygem(ethon) >= 0.7.0 rubygem(faraday)
BuildRequires:       rubygem(rack) rubygem(rspec) rubygem(sinatra) rubygem(webrick)
BuildRequires:       rubygem(redis) rubygem(dalli)
BuildArch:           noarch
%description
Like a modern code version of the mythical beast with 100 serpent heads,
Typhoeus runs HTTP requests in parallel while cleanly encapsulating handling
logic.

%package help
Summary:             Documentation for %{name}
Requires:            %{name} = %{version}-%{release}
BuildArch:           noarch
Provides:            %{name}-doc = %{version}-%{release}
Obsoletes:           %{name}-doc < %{version}-%{release}
%description help
Documentation for %{name}.

%prep
%setup -q -c -T
%gem_install -n %{SOURCE0}
%patch0 -p1

%build

%install
mkdir -p %{buildroot}%{gem_dir}
cp -a .%{gem_dir}/* \
        %{buildroot}%{gem_dir}/
for f in $(find %{buildroot}%{gem_instdir} -name \*.rb)
do
       sed -i -e '/^#!/d' $f
       chmod 0644 $f
done

%check
pushd .%{gem_instdir}
sed -i -e '/require "bundler"/ s/^/#/' \
  -e '/Bundler\.setup/ s/^/#/' spec/spec_helper.rb
rspec spec
popd

%files
%dir %{gem_instdir}
%{gem_libdir}
%exclude %{gem_instdir}/.*
%license %{gem_instdir}/LICENSE
%exclude %{gem_cache}
%{gem_spec}

%files help
%doc %{gem_instdir}/CONTRIBUTING.md
%doc %{gem_instdir}/CHANGELOG.md
%{gem_instdir}/Gemfile
%doc %{gem_instdir}/README.md
%{gem_instdir}/Guardfile
%{gem_instdir}/Rakefile
%doc %{gem_instdir}/UPGRADE.md
%{gem_instdir}/perf
%{gem_instdir}/spec
%{gem_instdir}/typhoeus.gemspec
%doc %{gem_docdir}

%changelog
* Fri Jun 07 2024 yaoxin <yao_xin001@hoperun.com> - 1.4.1-1
- Update to 1.4.1
  * Add global timeout and connecttimeout to config
  * Use bundle add instead

* Mon Nov 13 2023 liyanan <liyanan61@h-partners.com> - 1.4.0-1
- Update to 1.4.0
- Faraday adapter exceptions namespace compatibility with Faraday v1
- Add global config option `Typhoeus::Config.proxy`
- Add cache adapters for Dails,Rails, and Redis

* Thu Feb 24 2022 liyanan <liyanan32@huawei.com> - 1.0.2-2
- Fix Ruby 3.0 compatibility

* Mon Aug 24 2020 liyanan <liyanan32@huawei.com> - 1.0.2-1
- package init
